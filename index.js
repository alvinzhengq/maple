const Discord = require('discord.js');
const RateLimiter = require('discord.js-rate-limiter').RateLimiter;
const client = new Discord.Client();
const db = require('./db');
require('dotenv').config();

let rateLimiter = new RateLimiter(1, 1000);

client.on('message', msg => {
    if(msg.channel.name == 'maple' && msg.author.id != client.user.id){
        let splitMsg = msg.content.split("=");
        let isAdmin = msg.guild.member(msg.author).roles.cache.find(r => r.name == "Maple Admin") != undefined;
        let limited = rateLimiter.take(msg.author.id);

        if (limited) {
          return;
        }

        switch (splitMsg[0]) {
            case "harvest":
                db.harvest(msg.author.id, msg.guild.id, (res, win, days)=>{ 
                    msg.channel.send(res)
                    if(win){
                        msg.channel.send(`Congrulations ${msg.author.username}, you have reached 80000 points, you are the Maple Champion!`)
                        db.leaderboard(msg.guild.id, (res) => {
                            if(!Array.isArray(res)){msg.channel.send(res); return;}
                            if(res.length == 0){msg.channel.send("No players have harvested yet"); return;}

                            const promise = res.map(e => msg.guild.members.fetch({user: e.playerID}).then(r => r.displayName))
                            Promise.all(promise).then(usernames => {
                                let embed = new Discord.MessageEmbed()
                                .setTitle("**Maple Leaderboard**")
                                .setDescription("Point leaderboard for Maple. First to reach **80000** wins!")
                                .addFields(
                                    { name: 'Rank', value: Array.from({length: (res.length<=10?res.length:10)}, (_, i) => i + 1).join("\n"), inline: true },
                                    { name: 'Username', value: usernames.join("\n"), inline: true },
                                    { name: 'Points', value: res.map(e => e.points).join("\n") + "\n\u200b", inline: true }
                                )
                                .setFooter("Created by Cox#0008")

                                msg.channel.send(embed);

                                msg.channel.send("Ending game...")
                                db.stopGame(msg.guild.id, (res)=>{ msg.channel.send(res) });
                                db.addWin(msg.author.id, msg.guild.id, days);
                                msg.channel.send(msg.author.username + " has been added to this server's Hall Of Fame!")
                            })
                        })
                    }
                });
                break;

            case "nextharvest":
                if(splitMsg.length == 1){
                    db.nextHarvest(msg.author.id, msg.guild.id, (res)=>{ msg.channel.send(msg.author.username + "'s" + res) });
                }else{
                    msg.guild.members.fetch({query: splitMsg[1], limit: 1}).then(r => {
                        let reqUser = r.array().length==0?null:r.array()[0].id;
                        if(reqUser == null){
                            msg.channel.send("User not found")
                        }else{
                            db.nextHarvest(reqUser, msg.guild.id, (res)=>{ msg.channel.send(r.array()[0].displayName + "'s" + res) });
                        }
                    })
                }
                break;

            case "karma":
                if(splitMsg.length == 1){
                    db.karmaCmd(msg.author.id, msg.guild.id, (res)=>{ msg.channel.send(`${msg.author.username} has a ${res}% increased chance to get a 3x bonus`) });
                }else{
                    msg.guild.members.fetch({query: splitMsg[1], limit: 1}).then(r => {
                        let reqUser = r.array().length==0?null:r.array()[0].id;
                        if(reqUser == null){
                            msg.channel.send("User not found")
                        }else{
                            db.karmaCmd(reqUser, msg.guild.id, (res)=>{ msg.channel.send(`${r.array()[0].displayName} has a ${res}% increased chance to get a 3x bonus`) });
                        }
                    })
                }
                break;

            case "clock":
                db.clock(msg.guild.id, (res)=>{ msg.channel.send(res) })
                break;

            case "leaderboard":
                db.leaderboard(msg.guild.id, (res) => {
                    if(!Array.isArray(res)){msg.channel.send(res); return;}
                    if(res.length == 0){msg.channel.send("No players have harvested yet"); return;}

                    const promise = res.map(e => msg.guild.members.fetch({user: e.playerID}).then(r => r.displayName))
                    Promise.all(promise).then(usernames => {
                        let embed = new Discord.MessageEmbed()
                        .setTitle("**Maple Leaderboard**")
                        .setDescription("Point leaderboard for Maple. First to reach **80000** wins!")
                        .addFields(
                            { name: 'Rank', value: Array.from({length: (res.length<=10?res.length:10)}, (_, i) => i + 1).join("\n"), inline: true },
                            { name: 'Username', value: usernames.join("\n"), inline: true },
                            { name: 'Points', value: res.map(e => e.points).join("\n") + "\n\u200b", inline: true }
                        )
                        .setFooter("Created by Cox#0008")

                        msg.channel.send(embed);
                    })
                })

                break;
            
            case "score":
                if(splitMsg.length == 1){
                    db.score(msg.author.id, msg.guild.id, (res)=>{ if(res == null){msg.channel.send("You have not harvested yet")}else{msg.channel.send(res + " points")} });
                }else{
                    msg.guild.members.fetch({query: splitMsg[1], limit: 1}).then(r => {
                        let reqUser = r.array().length==0?null:r.array()[0].id;
                        if(reqUser == null){
                            msg.channel.send("User not found")
                        }else{
                            db.score(reqUser, msg.guild.id, (res)=>{ if(res == null){msg.channel.send("Player has not harvested yet")}else{msg.channel.send(res + " points")} });
                        }
                    })
                }
                break;

            case "halloffame":
                db.hallOfFame(msg.guild.id, (res) => {
                    if(res.length == 0){
                        msg.channel.send("No one has won in this server yet.")
                    }else{
						let cutOff = res.length - 20;
						if(cutOff < 0){cutOff = 0;}
						res = res.slice(0, 20)
                        const promise = res.map(e => msg.guild.members.fetch({user: e.user}).then(r => r.displayName))
                        Promise.all(promise).then(usernames => {
                            let embed = new Discord.MessageEmbed()
                            .setTitle("**Maple Hall Of Fame**")
                            .setDescription("Maple Champions of This Server")
                            .addFields(
                                { name: 'Username', value: usernames.join("\n"), inline: true },
                                { name: 'Date', value: res.map(e => {
                                    let a = new Date(parseInt(e.winDate)*1000)
                                    return `${a.getMonth()+1}/${a.getDate()}/${a.getFullYear()}`
                                }).join("\n"), inline: true},
                                { name: 'Won In', value: res.map(e => (parseFloat(e.days)/86400.0).toFixed(2).toString() + " Days").join("\n") + "\n\u200b", inline: true }
                            )
                            .setFooter(`Created by Cox#0457, ${cutOff} results cut off`)

                            msg.channel.send(embed);
                        })
                    }
                })
                break;

            case "send":
                if(splitMsg.length != 3){
                    msg.channel.send("Invalid send command")
                }else{
                    msg.guild.members.fetch({query: splitMsg[1], limit: 1}).then(r => {
                        let reqUser = r.array().length==0?null:r.array()[0].id;
                        if(reqUser == null){
                            msg.channel.send("User not found")
                        }else{
                            if(parseInt(splitMsg[2]) != 0 && Number.isInteger(parseInt(splitMsg[2])) && parseInt(splitMsg[2]) <= 80000 && reqUser != msg.author.id){
                                if(parseInt(splitMsg[2]) < 100){
                                    msg.channel.send("You must send a minimum of 100 points")
                                }else{
                                    db.give(msg.guild.id, msg.author.id, reqUser, splitMsg[2], (res) => {msg.channel.send(res)});
                                }
                            }else{
                                msg.channel.send("Invalid send command")
                            }
                        }
                    })
                }
                break;

            case "startgame":
                if(!isAdmin){
                    msg.channel.send("You are not authorized to start a game");
                }else{
                    db.startGame(msg.guild.id, (res)=>{ msg.channel.send(res) });
                }
                break;

            case "endgame":
                if(!isAdmin){
                    msg.channel.send("You are not authorized to stop a game");
                }else{
                    db.stopGame(msg.guild.id, (res)=>{ msg.channel.send(res) });
                }
                break;

            case "help":
                let embed = new Discord.MessageEmbed()
                .setTitle("**Maple Help**")
                .setDescription("**How to play**: \n \
                Harvest for points, the amount of points you gain is the seconds since the last harvest by *any* player, you want to maximize this time to get the most amount of points. But be careful, people can *snipe* you by harvesting right before you, causing you to waste your harvest on very few points. You can also send people some of your points to recieve karma, which will increase your chance to get a 3x bonus on your harvests, the amount recieved by the other person is taxed so it will not be the full amount you specified. First person to get to **80000** points wins." + "\n\u200b")
                .addFields(
                    { name: 'Game Commands', value: "```- Harvest: harvest\n- Time Since Last Harvest: clock\n- Send Another Person Points to Recieve Karma: send=[name]=[amount]\n- Get Your Harvest Cooldown: nextharvest\n- Query a Person's Harvest Cooldown: nextharvest=[name]\n\- Get Your Score: score\n- Query Another Person's Score: score=[name]\n- Get Your Karma: karma\n- Query Another Person's Karma: karma=[name]\n- Leaderboard: leaderboard\n- Hall Of Fame: halloffame```" },
                    { name: 'Start and End Game(Maple Admin role only)', value: "```- Start Game: startgame\n- End Game Forcefully: endgame```" + "\u200b" },
                )
                .setFooter("Created by Cox#0008")

                msg.channel.send(embed);
                break;
        
            default:
                break;
        }
    }
});

client.on('ready', () => {
    client.user.setActivity("Harvesting Syrup 🪓"); 
    console.log(`Logged in as ${client.user.tag}!`);
});
client.login(process.env.TOKEN);
