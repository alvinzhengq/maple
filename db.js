require('dotenv').config();
var MongoClient = require('mongodb').MongoClient;
const url = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@maplegamedata.n8ivf.mongodb.net/gameData?retryWrites=true&w=majority`

function harvest(author, guild, cb){
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("gameData");

        dbo.collection(guild).findOne({ "lastHarvest": {$exists: true} }, (err, resG) => {
            if(err) throw err;
            if(resG == null){
                cb("No active games in this server");
                db.close();
                return;
            }

            var pointsGained = Math.abs(Math.floor(Date.now()/1000) - parseInt(resG.lastHarvest));
            var playerQuery = { "playerID": author };
            var bonus = 1;

            dbo.collection(guild).findOne(playerQuery, (err, resP) => {
                if(resP == null) {
                    let rando = Math.random();
                    if(rando < 0.1){
                        bonus = 3;
                        pointsGained *= 3;
                    }else if(rando < 0.25) {
                        bonus = 2;
                        pointsGained *= 2;
                    }
        
                    dbo.collection(guild).insertOne({ "playerID": author, "points": pointsGained, "cooldown": (Math.floor(Date.now()/1000)+1800).toString(), "karma": 0.0 }, (err, res) => {
                        dbo.collection(guild).updateOne(
                        { "lastHarvest": {$exists: true} }, {$set: {"lastHarvest": Math.floor(Date.now()/1000).toString()}}, (err, res) => {
                            if(bonus == 3){
                                cb("You gained " + pointsGained + " points, with a 3x bonus", pointsGained >= 80000, Math.abs(Math.floor(Date.now()/1000) - parseInt(resG.gameStart)));
                            }else if(bonus == 2){
                                cb("You gained " + pointsGained + " points, with a 2x bonus", pointsGained >= 80000, Math.abs(Math.floor(Date.now()/1000) - parseInt(resG.gameStart)));
                            }else{
                                cb("You gained " + pointsGained + " points", pointsGained >= 80000, Math.abs(Math.floor(Date.now()/1000) - parseInt(resG.gameStart)));
                            }
                            db.close();
                        });
                    });
                }else{
                    if(parseInt(resP.cooldown) > Math.floor(Date.now()/1000)) {
                        let diff = parseInt(resP.cooldown)-Math.floor(Date.now()/1000);
                        cb(`Your harvest is on cooldown, please wait ${Math.floor(diff/60)}m, ${diff%60}s`);
                        db.close();
                        return;
                    }
                    
                    let rando = Math.random();
                    if(rando < (0.1)+resP.karma){
                        bonus = 3;
                        pointsGained *= 3;
                    }else if(rando < (0.25)+resP.karma) {
                        bonus = 2;
                        pointsGained *= 2;
                    }
        
                    dbo.collection(guild).updateOne(playerQuery, {$inc: { "points": pointsGained }, $set: {"cooldown": (Math.floor(Date.now()/1000)+1800).toString()}},(err, res) => {
                        dbo.collection(guild).updateOne(
                        { "lastHarvest": {$exists: true} }, {$set: {"lastHarvest": Math.floor(Date.now()/1000).toString()}}, (err, res) => {
                            if(bonus == 3){
                                cb("You gained " + pointsGained + " points, with a 3x bonus", resP.points+pointsGained >= 80000, Math.abs(Math.floor(Date.now()/1000) - parseInt(resG.gameStart)));
                            }else if(bonus == 2){
                                cb("You gained " + pointsGained + " points, with a 2x bonus", resP.points+pointsGained >= 80000, Math.abs(Math.floor(Date.now()/1000) - parseInt(resG.gameStart)));
                            }else{
                                cb("You gained " + pointsGained + " points", resP.points+pointsGained >= 80000, Math.abs(Math.floor(Date.now()/1000) - parseInt(resG.gameStart)));
                            }
                            db.close();
                        });
                    });
                }
            });
        })
    });
}

function startGame(guild, cb){
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("gameData");

        dbo.collection(guild).findOne({ "lastHarvest": {$exists: true} }, (err, res) => {
            if(err) throw err;
            if(res != null) {
                cb("Game already started in this server");
                db.close();
                return;
            }

            dbo.collection(guild).insertOne({"lastHarvest": Math.floor(Date.now()/1000).toString(), "gameStart": Math.floor(Date.now()/1000).toString()}, (err, res) => {
                if(err) throw err;
                cb("Game started successfully, timer starting now.")
                db.close();
            })
        })
    });
}

function stopGame(guild, cb){
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("gameData");

        dbo.collection(guild).findOne({ "lastHarvest": {$exists: true} }, (err, res) => {
            if(err) throw err;
            if(res == null){
                cb("No active games in this server");
                db.close();
                return;
            }

            dbo.collection(guild).drop((err, res) => {
                if(err) throw err;
                cb("Game ended successfully, data deleted from database.")
                db.close();
            })
        })
    });
}

function nextHarvest(user, guild, cb){
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("gameData");

        dbo.collection(guild).findOne({ "playerID": user }, (err, res) => {
            if(res == null){
                cb(` harvest is not on cooldown`);
                db.close();
                return;
            }

            if(parseInt(res.cooldown) > Math.floor(Date.now()/1000)){
                let diff = parseInt(res.cooldown)-Math.floor(Date.now()/1000);
                cb(` next harvest is in ${Math.floor(diff/60)}m, ${diff%60}s`);
                db.close();
                return;
            }else{
                cb(` harvest is not on cooldown`);
                db.close();
                return;
            }
        })
    })
}

function clock(guild, cb){
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("gameData");

        dbo.collection(guild).findOne({ "lastHarvest": {$exists: true} }, (err, res) => {
            if(err) throw err;
            if(res == null) {
                cb("No active games in this server");
                db.close();
                return;
            }

            var pointsGained = Math.abs(Math.floor(Date.now()/1000) - parseInt(res.lastHarvest));
            cb("Current harvest time is " + pointsGained + " seconds.")
            db.close();
        })
    })
}

function leaderboard(guild, cb){
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("gameData");

        dbo.collection(guild).findOne({ "lastHarvest": {$exists: true} }, (err, res) => {
            if(err) throw err;
            if(res == null){
                cb("No active games in this server");
                db.close();
                return;
            }

            dbo.collection(guild).find({ "points": {$exists: true} }).limit(5).sort({ "points": -1 }).toArray((err, res) => {
                if(err) throw err;
                cb(res);
                db.close();
            })
        })
    });
}

function score(user, guild, cb){
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("gameData");

        dbo.collection(guild).findOne({ "lastHarvest": {$exists: true} }, (err, res) => {
            if(err) throw err;
            if(res == null){
                cb("No active games in this server");
                db.close();
                return;
            }

            dbo.collection(guild).findOne({ "playerID": user }, (err, res) => {
                if(err) throw err;
                if(res == null){
                    cb(null);
                }else{
                    cb(res.points);
                }
                db.close();
            })
        })
    });
}

function addWin(user, guild, days){
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("gameData");

        dbo.collection("globalData").findOne({ "guildID": guild }, (err, res) => {
            if(err) throw err;

            if(res == null){
                dbo.collection("globalData").insertOne({ "guildID": guild, wins: [{"user": user, "days": days, "winDate": Math.floor(Date.now()/1000).toString()}] }, (err, res) => {
                    if(err) throw err;
                    db.close();
                })
            }else{
                dbo.collection("globalData").updateOne({ "guildID": guild }, {$push: {wins: {"user": user, "days": days, "winDate": Math.floor(Date.now()/1000).toString()}}},(err, res) => {
                    if(err) throw err;
                    db.close();
                })
            }
        })
    });
}

function compare( a, b ) {
    if ( a.days < b.days ){
      return -1;
    }else if ( a.days > b.days ){
      return 1;
    }
    return 0;
}

function hallOfFame(guild, cb){
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("gameData");

        dbo.collection("globalData").findOne({ "guildID": guild }, (err, res) => {
            if(err) throw err;
            if(res == null){
                cb([]);
            }else{
                res.wins.sort(compare);
                cb(res.wins);
            }
        })
    });
}

function karma(x){
    return (Math.sqrt(1.5)*x)/(119145.116323);	
}

function degrade(x){
    return x * ((0.86)**Math.sqrt(3))
}

function roundNumber(num, scale) {
    if(!("" + num).includes("e")) {
      return +(Math.round(num + "e+" + scale)  + "e-" + scale);
    } else {
      var arr = ("" + num).split("e");
      var sig = ""
      if(+arr[1] + scale > 0) {
        sig = "+";
      }
      return +(Math.round(+arr[0] + "e" + sig + (+arr[1] + scale)) + "e-" + scale);
    }
}

function give(guild, giver, reciever, amount, cb){
    let karmaP = roundNumber(karma(parseInt(amount)), 4);
    let toGive = Math.ceil(degrade(parseInt(amount)));

    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("gameData");

        dbo.collection(guild).findOne({ "playerID": giver }, (err, res) => {
            if(err) throw err;
            if(res == null || res.points < amount){
                cb("You do not have enough points to give");
                db.close();
            }else{
                dbo.collection(guild).findOne({ "playerID": reciever }, (err, res) => {
                    if(res == null){
                        dbo.collection(guild).insertOne({ "playerID": reciever, "points": toGive, "cooldown": 0, "karma": 0.0 }, (err, res) => {
                            dbo.collection(guild).updateOne({ "playerID": giver }, {$inc: { "points": -parseInt(amount), "karma": karmaP }}, (err, res) => {
                                cb(`Transaction successful, you have transferred ${toGive} points to the other player, and recieved ${(karmaP*100).toFixed(2)}% increase in luck.`)
                                db.close();
                            })
                        })
                    }else{
                        dbo.collection(guild).updateOne({ "playerID": reciever }, {$inc: { "points": toGive }},(err, res) => {
                            dbo.collection(guild).updateOne({ "playerID": giver }, {$inc: { "points": -parseInt(amount), "karma": karmaP }}, (err, res) => {
                                cb(`Transaction successful, you have transferred ${toGive} points after taxes to the other player, and recieved ${(karmaP*100).toFixed(2)}% increase in luck.`)
                                db.close();
                            })
                        })
                    }
                })
            }

        })
    });
}

function karmaCmd(user, guild, cb){
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("gameData");

        dbo.collection(guild).findOne({ "playerID": user }, (err, res) => {
            if(res == null){
                cb(0);
            }else{
                cb((parseFloat(res.karma)*100).toFixed(2));
            }
            db.close();
        })
    })
}

module.exports = {
    harvest,
    nextHarvest,
    clock,
    leaderboard,
    startGame,
    stopGame,
    score,
    addWin,
    hallOfFame,
    give,
    karmaCmd
}
